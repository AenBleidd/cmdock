/***********************************************************************
 * The rDock program was developed from 1998 - 2006 by the software team
 * at RiboTargets (subsequently Vernalis (R&D) Ltd).
 * In 2006, the software was licensed to the University of York for
 * maintenance and distribution.
 * In 2012, Vernalis and the University of York agreed to release the
 * program as Open Source software.
 * This version is licensed under GNU-LGPL version 3.0 with support from
 * the University of Barcelona.
 * http://rdock.sourceforge.net/
 ***********************************************************************/

// Miscellaneous string constants

#ifndef _RBTRESOURCES_H_
#define _RBTRESOURCES_H_

#include <string>

const std::string IDS_NAME = "CmDock";

#if defined(_WIN32) || defined(__FreeBSD__)
const std::string IDS_COPYRIGHT =
    "             _____      ___           __\n"
    "            / ___/_ _  / _ \\___  ____/ /__\n"
    "           / /__/  ' \\/ // / _ \\/ __/  '_/\n"
    "           \\___/_/_/_/____/\\___/\\__/_/\\_\\\n\n"
    "********************************************************\n"
    "The " + IDS_NAME + " molecular docking program is\n"
    "licensed under GNU LGPL version 3. " + IDS_NAME + "\n"
    "is maintained by Marko Jukic, Nejc Ilc, Davor Sluga,\n"
    "Crtomir Podlipnik, and Gasper Tomsic.\n"
    "Visit https://gitlab.com/Jukic/cmdock/ for more info.\n"
    "Good Luck and Good Health!          CmDock team           "
    "\n********************************************************";
#else
const std::string IDS_COPYRIGHT =
    "             _____      ___           __\n"
    "            / ___/_ _  / _ \\___  ____/ /__\n"
    "           / /__/  ' \\/ // / _ \\/ __/  '_/\n"
    "           \\___/_/_/_/____/\\___/\\__/_/\\_\\\n\n"
    "********************************************************\n"
    "The " + IDS_NAME + " molecular docking program is\n"
    "licensed under GNU LGPL version 3. " + IDS_NAME + "\n"
    "is maintained by Marko Jukić, Nejc Ilc, Davor Sluga,\n"
    "Črtomir Podlipnik, and Gašper Tomšič.\n"
    "Visit https://gitlab.com/Jukic/cmdock/ for more info.\n"
    "Good Luck and Good Health!          CmDock team           "
    "\n********************************************************";
#endif

const std::string IDS_PRODUCT = "libcmdock.so";

const std::string IDS_VERSION = "0.1.4";

#endif //_RBTRESOURCES_H_
