# CmDock: update on a fast, versatile, and open-source program for docking ligands to proteins and nucleic acids

<img src="docs/_static/logo-wide.svg" width="500">



CmDock is a collection of optimisations, QoL and parallelism updates on a
fast and versatile **open-source** docking software that can be used
to dock **ligands** against **proteins** and **nucleic acids**. Software is capable
of high-throughput virtual screening (HTVS) campaigns and binding mode
prediction studies.

CmDock is named after Curie Marie and is dedicated to all women in science! 
(Curium is a transuranic radioactive chemical element with the symbol Cm and atomic number 96. 
This element of the actinide series was named after Marie and Pierre Curie, both known for their research on radioactivity.)

## About

### Features

  - Docking preparation
  - Pre-processing of input files
  - High-throughput virtual screening experiment design
  - High-throughput virtual screening
  - Binding mode prediction
  - Post-processing and analysis of results
  - Definable Checkpoints
  - Calculation Progress Monitoring (all platforms)
  - Support for compressed input and output via cmzip
  - Definable runs (1M) and output best scoring poses

  (-  relative paths in target.prm definitions)

### History

The RiboDock program was developed from 1998 to 2006 by the software team at
RiboTargets (subsequently [Vernalis (R&D) Ltd](https://www.vernalis.com/)).
In 2006, the software was licensed to the
[University of York](http://www.ysbl.york.ac.uk/) for maintenance and
distribution under the name rDock.

In 2012, Vernalis and the University of York agreed to release the program as
open-source software. This version is developed with support from the
[University of Barcelona](http://www.ub.edu/cbdd/) --
[sourceforge.net/projects/rdock](http://sourceforge.net/projects/rdock/).

The development of rDock stalled in 2014. Since 2019,
[RxTx](https://www.rxtx.tech/) is developing a fork of rDock under the name
RxDock.

In order to optimise, implement new features and utilise modern hardware, from 2020, a fork
of RxDock is developed under the name **CmDock**.

### Languages & Platforms

- C++ (main)
- Python
- Rust
- (Perl)

platforms: Linux, Mac, Windows, ARM


### References (Please cite!)

Bahun, M., Jukić, M., Oblak, D., Kranjc, L.,
 Bajc, G., Butala, M., ... & Ulrih, N. P. (2021). Inhibition of the SARS-CoV-2 
 3CLpro main protease by plant polyphenols. Food Chemistry, 131594.
 [doi.org/10.1016/j.foodchem.2021.131594](https://www.sciencedirect.com/science/article/pii/S0308814621026005?via%3Dihub)

Jukič, M., Škrlj, B., Tomšič, G.,
  Pleško, S., Podlipnik Č., Bren, U. (2021) Prioritisation of Compounds for 3CLpro 
  Inhibitor Development on SARS-CoV-2 Variants. Molecules 
  26(10): 3003.
  [doi.org/10.3390/molecules26103003](https://www.mdpi.com/1420-3049/26/10/3003)

Ruiz-Carmona, S., Alvarez-Garcia, D., Foloppe, N.,
  Garmendia-Doval, A. B., Juhos S., et al. (2014) rDock: A Fast, Versatile and
  Open Source Program for Docking Ligands to Proteins and Nucleic Acids. PLoS
  Comput Biol 10(4): e1003571.
  [doi:10.1371/journal.pcbi.1003571](https://doi.org/10.1371/journal.pcbi.1003571)

Morley, S. D. and Afshar, M. (2004) Validation of an empirical
  RNA-ligand scoring function for fast flexible docking using RiboDock®. J
  Comput Aided Mol Des, 18: 189--208.
  [doi:10.1023/B:JCAM.0000035199.48747.1e](https://doi.org/10.1023/B:JCAM.0000035199.48747.1e)

### Past Software/Methodology Applications

Soler, D., Westermaier, Y., & Soliva, R. (2019). Extensive benchmark of rDock as a peptide-protein docking tool. 
J Comput Aided Mol Des, 33(7), 613-626. (https://doi.org/10.1007/s10822-019-00212-0)

Fulle, S., & Gohlke, H. (2010). Molecular recognition of RNA: challenges for modelling interactions and plasticity. 
J Mol Recognit, 23(2), 220-231. (https://doi.org/10.1002/jmr.1000)
