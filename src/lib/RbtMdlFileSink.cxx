/***********************************************************************
 * The rDock program was developed from 1998 - 2006 by the software team
 * at RiboTargets (subsequently Vernalis (R&D) Ltd).
 * In 2006, the software was licensed to the University of York for
 * maintenance and distribution.
 * In 2012, Vernalis and the University of York agreed to release the
 * program as Open Source software.
 * This version is licensed under GNU-LGPL version 3.0 with support from
 * the University of Barcelona.
 * http://rdock.sourceforge.net/
 ***********************************************************************/

#include <iomanip>
#include <string>

#include "RbtMdlFileSink.h"
#include "RbtFileError.h"

////////////////////////////////////////
// Constructors/destructors
RbtMdlFileSink::RbtMdlFileSink(const std::string &fileName, RbtModelPtr spModel, bool checkpointMode)
    : RbtBaseMolecularFileSink(fileName, spModel), m_bFirstRender(true) {
  // DM 27 Apr 1999 - default behaviour is for the first Render to overwrite any
  // existing file then subsequent Renders to append.
  SetAppend(checkpointMode);
  m_bCheckpointMode = checkpointMode;
  
  // Open an Element data source
  m_spElementData = RbtElementFileSourcePtr(
      new RbtElementFileSource(Rbt::GetRbtFileName("data", "RbtElements.dat")));
  _RBTOBJECTCOUNTER_CONSTR_("RbtMdlFileSink");
}

RbtMdlFileSink::~RbtMdlFileSink() {
  _RBTOBJECTCOUNTER_DESTR_("RbtMdlFileSink");
}

////////////////////////////////////////
// Public methods
////////////////
//
////////////////////////////////////////
// Override public methods from RbtBaseFileSink
void RbtMdlFileSink::Render() {
  try {
    RbtModelPtr spModel(GetModel());
    RbtAtomList modelAtomList(spModel->GetAtomList());
    RbtBondList modelBondList(spModel->GetBondList());
    RbtModelList solventList = GetSolvent();
    // Concatenate all solvent atoms and bonds into a single list
    // DM 7 June 2006 - only render the enabled solvent models
    RbtAtomList solventAtomList;
    RbtBondList solventBondList;
    for (RbtModelListConstIter iter = solventList.begin();
         iter != solventList.end(); ++iter) {
      if ((*iter)->GetEnabled()) {
        RbtAtomList atomList = (*iter)->GetAtomList();
        RbtBondList bondList = (*iter)->GetBondList();
        std::copy(atomList.begin(), atomList.end(),
                  std::back_inserter(solventAtomList));
        std::copy(bondList.begin(), bondList.end(),
                  std::back_inserter(solventBondList));
      }
    }

    // Title lines
    std::vector<std::string> titleList(spModel->GetTitleList());
    if (!titleList.empty())
      AddLine(titleList[0]);
    else
      AddLine(spModel->GetName());

    // Molfile allows up to 8 characters for program name
    std::string programName = Rbt::GetProgramName();
    if (programName.length() > 8)
      programName = programName.substr(0, 8);
    programName.insert(programName.length(), 8 - programName.length(), ' ');

    // File timestamp is 10 characters
    std::time_t t = std::time(nullptr);
    char ftime[11];
    std::strftime(ftime, sizeof(ftime), "%m%d%y%H%M", std::localtime(&t));

    // First two characters are user initials, leave them empty for now
    AddLine("  " + programName + std::string(ftime) + "3D");
    AddLine(Rbt::GetProduct() + "/" + Rbt::GetVersion());

    // Write number of atoms and bonds
    std::ostringstream ostr;
    ostr << std::setw(3) << modelAtomList.size() + solventAtomList.size()
         << std::setw(3) << modelBondList.size() + solventBondList.size()
         << std::setw(3) << 0 << std::setw(3) << 0 << std::setw(3) << 0
         << std::setw(3) << 0 << std::setw(3) << 0 << std::setw(3) << 0
         << std::setw(3) << 0 << std::setw(3) << 0 << std::setw(3) << 999
         << " V2000";
    AddLine(ostr.str());

    // DM 19 June 2006 - clear the map of logical atom IDs each time
    // we render a model
    m_atomIdMap.clear();
    RenderAtomList(modelAtomList);
    RenderAtomList(solventAtomList);
    RenderBondList(modelBondList);
    RenderBondList(solventBondList);
    RenderData(spModel->GetDataMap());

    AddLine("$$$$");

    if (m_bBestPoses) {
      try {
        double data = std::stod(GetFieldData(spModel->GetDataMap(), m_strBestPoseCriterium));
        RbtHeap::Ligand ligand(data, m_lineRecs);
        m_ligandHeap.push(ligand);
        m_lineRecs.clear();
      } catch (...) {
        std::cerr << "Data in " << m_strBestPoseCriterium << " is not a number!" << std::endl;
        exit(1);
      }
    } else {
      Write();
      if (m_bFirstRender) {
        SetAppend(true);
        m_bFirstRender = false;
      }
    }
  } catch (RbtError &error) {
    // Close();
    throw; // Rethrow the RbtError
  }
}

void RbtMdlFileSink::RenderAtomList(const RbtAtomList &atomList) {
  for (RbtAtomListConstIter aIter = atomList.begin(); aIter != atomList.end();
       aIter++) {
    RbtAtomPtr spAtom(*aIter);
    // DM 19 June 2006. Check if this atom has been rendered previously.
    if (m_atomIdMap.find(spAtom) == m_atomIdMap.end()) {
      unsigned int nextAtomId = m_atomIdMap.size() + 1;
      // std::cout << "RenderAtom " << spAtom->GetFullAtomName() << " (actual
      // ID=" << spAtom->GetAtomId()
      //	 << "); file ID=" << nextAtomId << std::endl;
      m_atomIdMap.insert(std::make_pair(spAtom, nextAtomId));
    } else {
      // Should never happen. Probably best to throw an error at this point.
      throw RbtBadArgument(_WHERE_, "Duplicate atom rendered: " +
                                        spAtom->GetFullAtomName());
    }
    RbtElementData elData =
        m_spElementData->GetElementData(spAtom->GetAtomicNo());
    int nFormalCharge = spAtom->GetFormalCharge();
    if (nFormalCharge != 0)
      nFormalCharge = 4 - nFormalCharge;
    std::ostringstream ostr;
    ostr.precision(4);
    ostr.setf(std::ios_base::fixed, std::ios_base::floatfield);
    ostr.setf(std::ios_base::right, std::ios_base::adjustfield);
    ostr << std::setw(10) << spAtom->GetX() << std::setw(10) << spAtom->GetY()
         << std::setw(10) << spAtom->GetZ(); // X,Y,Z coord
    ostr.setf(std::ios_base::left, std::ios_base::adjustfield);
    ostr << std::setw(0) << " " << std::setw(3)
         << elData.element.c_str(); // Element name
    ostr.setf(std::ios_base::right, std::ios_base::adjustfield);
    ostr << std::setw(2) << 0             // mass difference
         << std::setw(3) << nFormalCharge // charge
         << std::setw(3) << 0             // atom stereo parity
         << std::setw(3) << 0             // hydrogen count+1 (query CTABs only)
         << std::setw(3) << 0             // stereo care box (query CTABs only)
         << std::setw(3) << 0;            // valence (0 = no marking)
    // Mass diff, formal charge, stereo parity, num hydrogens,
    // center
    AddLine(ostr.str());
  }
}

void RbtMdlFileSink::RenderBondList(const RbtBondList &bondList) {
  for (RbtBondListConstIter bIter = bondList.begin(); bIter != bondList.end();
       bIter++) {
    RbtBondPtr spBond(*bIter);
    // DM 19 June 2006. Lookup the logical atom IDs for each atom in the bond
    RbtAtomIdMap::const_iterator aIter1 =
        m_atomIdMap.find(spBond->GetAtom1Ptr());
    RbtAtomIdMap::const_iterator aIter2 =
        m_atomIdMap.find(spBond->GetAtom2Ptr());
    if ((aIter1 != m_atomIdMap.end()) && (aIter2 != m_atomIdMap.end())) {
      unsigned int id1 = (*aIter1).second;
      unsigned int id2 = (*aIter2).second;
      // std::cout << "RenderBond " << spBond->GetAtom1Ptr()->GetFullAtomName()
      //	 << spBond->GetAtom2Ptr()->GetFullAtomName()
      //	 << "; file ID1=" << id1
      //	 << "; file ID2=" << id2 << std::endl;
      std::ostringstream ostr;
      ostr.setf(std::ios_base::right, std::ios_base::adjustfield);
      ostr << std::setw(3) << id1 << std::setw(3) << id2 << std::setw(3)
           << spBond->GetFormalBondOrder() << std::setw(3) << 0 << std::setw(3)
           << 0 << std::setw(3) << 0; // Atom1, Atom2, bond order, stereo
                                      // designator, unused, topology code
      AddLine(ostr.str());
    } else {
      // Should never happen. Probably best to throw an error at this point.
      throw RbtBadArgument(_WHERE_,
                           "Error rendering bond, logical atom IDs not found");
    }
  }
}

void RbtMdlFileSink::RenderData(const RbtStringVariantMap &dataMap) {
  if (!dataMap.empty()) {
    AddLine("M  END"); // End of molecule marker
    for (RbtStringVariantMapConstIter iter = dataMap.begin();
         iter != dataMap.end(); iter++) {
      AddLine(">  <" + (*iter).first + ">"); // Field name
      std::vector<std::string> sl = (*iter).second.StringList();
      for (std::vector<std::string>::const_iterator slIter = sl.begin();
           slIter != sl.end(); ++slIter) {
        AddLine(*slIter); // Field values
      }
      AddLine(""); // Blank line denotes end of field values
      if (std::find(m_convertToKCAL.begin(), m_convertToKCAL.end(), (*iter).first) != m_convertToKCAL.end()) {
        std::string kcalMol = std::to_string(std::stod(sl[0]) * 0.238846);
        AddLine(">  <" + (*iter).first + ".KCAL>");
        AddLine(kcalMol);
        AddLine("");
      }
    }
  }
}

std::string RbtMdlFileSink::GetFieldData(const RbtStringVariantMap &dataMap, std::string fieldName) {
  if (!dataMap.empty()) {
    std::string output = "";
    for (RbtStringVariantMapConstIter iter = dataMap.begin();
        iter != dataMap.end(); iter++) {
      if ((*iter).first == fieldName) {
        std::vector<std::string> sl = (*iter).second.StringList();
        for (auto line : sl) {
          output += line + "\n";
        }
        break;
      }
    }
    return output;
  } else {
    return "";
  }
}

void RbtMdlFileSink::Write(bool bClearCache) {
  // Only write the file if there is anything in the cache
  if (isCacheEmpty())
    return;

  try {
    Open(m_bAppend); // DM 06 Apr 1999 - open for append or overwrite, depending
                     // on m_bAppend attribute
    if (m_bZip) {
      for (std::string line : m_lineRecs) {
        std::string delimiter = "$$$$";
        std::string cLine = line + "\n"; 
        if (cLine.rfind(delimiter, 0) != 0) {
            m_archive.decompressedRecord.insert(std::end(m_archive.decompressedRecord), cLine.c_str(), cLine.c_str() + strlen(cLine.c_str()));
            continue; // Only finish loop when $$$$ is reached, this way we compress each record by itself
        }
        m_archive.decompressedRecord.insert(std::end(m_archive.decompressedRecord), cLine.c_str(), cLine.c_str() + strlen(cLine.c_str()));
        
        // Compress record with specified compression level
        if (!m_archive.compress()) {
            eprintln("Error compressing record!");
            exit(1);
        };
        m_archive.decompressedRecord.clear();

        // Add compressed record to index
        if (!m_archive.addCurrentToIndex()) {
            eprintln("Error adding to index!");
            exit(1);
        };

        // Write record to file
        if (!m_archive.writeRecordToFile()) {
            eprintln("Error writing to file!");
            exit(1);
        };
      }
    } else {
      for (std::vector<std::string>::const_iterator iter = m_lineRecs.begin();
          iter != m_lineRecs.end(); iter++) {
        // for some reason the << overload is screwed up in some sstream
        // implementations so it is worth to pay this "pointless" price in
        // conversion
        std::string delimited((*iter).c_str());
        m_fileOut << delimited << std::endl;
        m_fileOut.flush();
        // m_fileOut << *iter << std::endl;
      }
    }
    if (bClearCache)
      ClearCache(); // Clear the cache so we don't write the file again
  }

  // Got an RbtError
  catch (RbtError &error) {
    Close();
    if (bClearCache)
      ClearCache();
    throw;
  }
}

void RbtMdlFileSink::WriteBestPoses(bool bClearCache) {
  while (!m_ligandHeap.empty()) {
    m_lineRecs.clear();
    m_lineRecs.resize(m_ligandHeap.top().ligand.size());
    m_lineRecs = m_ligandHeap.top().ligand;

    Write(bClearCache);
    m_ligandHeap.pop();
  }
  if (bClearCache)
    m_ligandHeap.clear();
}

////////////////////////////////////////
// Private methods
/////////////////
void RbtMdlFileSink::Open(bool bAppend) {
  if (!m_bFileOpen) {
    std::ios_base::openmode openMode = std::ios_base::out;
    if (bAppend || m_bCheckpointMode)
      openMode = openMode | std::ios_base::app;
    if (m_bZip) {
      openMode = openMode | std::ios_base::binary;
      m_archive.outputFile.open(m_strFileName.c_str(), openMode);
      if (m_bFirstOpen && !CmZ::FileIsEmpty(m_strFileName.c_str())) {
        if (m_bCheckpointMode && m_archive.outputFile.good()) {
          m_archive.outputFile.close();
          m_archive.index = CmZ::rebuildIndex(m_strFileName.c_str());
          m_archive.outputFile.open(m_strFileName.c_str(),
            std::ios_base::in | std::ios_base::out | std::ios_base::binary);
          if (!(m_archive.index.size() < 2)) { // Check if there's any records
            m_archive.inputFile.open(m_strFileName, std::ios_base::binary);
            if (m_archive.readRecordAtIndex(0) && m_archive.decompress()) { // Check if first record is valid
              if (m_archive.readRecordAtIndex(UINT64_MAX) && m_archive.decompress()) { // Check if last record is index
                bool bIsValidMdlHeader = CmZ::isValidMdlHeader(m_archive.decompressedRecord);
                m_archive.index.pop_back();
                m_archive.inputFile.close();
                if (!bIsValidMdlHeader) {
                  m_archive.outputFile.seekp(m_archive.calculateOffset(UINT64_MAX), std::ios_base::beg);
                } else {
                  m_archive.outputFile.seekp(0, std::ios_base::end);
                }
              } 
            }
          } else {
            m_archive.outputFile.seekp(0, std::ios_base::beg);
            m_archive.index = {};
          }
        }
      }
    } else {
      m_fileOut.open(m_strFileName.c_str(), openMode);
    }
  }
  if (!(m_fileOut || m_archive.outputFile)){
    throw RbtFileWriteError(_WHERE_, "Error opening " + m_strFileName);
  } else {
    m_bFileOpen = true;
    if (m_bFirstOpen)
      m_bFirstOpen = false;
  }
}

void RbtMdlFileSink::Close() { 
  if (m_bZip) {
    m_archive.outputFile.close();
  } else {
    m_fileOut.close();
  }
  m_bFileOpen = false;
}