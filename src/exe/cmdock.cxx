/***********************************************************************
 * The rDock program was developed from 1998 - 2006 by the software team
 * at RiboTargets (subsequently Vernalis (R&D) Ltd).
 * In 2006, the software was licensed to the University of York for
 * maintenance and distribution.
 * In 2012, Vernalis and the University of York agreed to release the
 * program as Open Source software.
 * Version with support from the University of Barcelona is found at:
 * http://rdock.sourceforge.net/
 * From 2019 RxTx developed a fork of rDock 
 * called RxDock (modern rewrite) at: https://gitlab.com/rxdock
 * From 2020 a fork of RxDock is developed under the name CmDock
 * or CurieMarieDock at: https://gitlab.com/Jukic/cmdock
 * **************licensed under GNU-LGPL version 3.0********************
 ***********************************************************************/

// Main docking application
#include <cerrno>
#include <chrono>
#include <cxxopts.hpp>
#include <iomanip>
#include <sstream>
#include <cstdio>
#include <signal.h>
#ifdef _WIN32
#include <indicators/progress_bar.hpp>
#ifndef NOMINMAX
#define NOMINMAX
#endif
#else
#include <indicators/block_progress_bar.hpp>
#endif
#include <indicators/cursor_control.hpp>
#include <composestream.hpp>

#include "RbtBiMolWorkSpace.h"
#include "RbtCrdFileSink.h"
#include "RbtCompression.hpp"
#include "RbtDockingError.h"
#include "RbtFileError.h"
#include "RbtFilter.h"
#include "RbtLigandError.h"
#include "RbtMdlFileSink.h"
#include "RbtMdlFileSource.h"
#include "RbtModelError.h"
#include "RbtPRMFactory.h"
#include "RbtParameterFileSource.h"
#include "RbtRand.h"
#include "RbtSFFactory.h"
#include "RbtSFRequest.h"
#include "RbtTransformFactory.h"

// Section name in docking prm file containing scoring function definition
const std::string _ROOT_SF = "SCORE";
const std::string _RESTRAINT_SF = "RESTR";
const std::string _ROOT_TRANSFORM = "DOCK";

// Termination handler, lets kill gracefully
void terminator(int s) {
  std::cout << std::endl << "Caught termination signal, wrapping up..." << std::endl;
  indicators::show_console_cursor(true);
  exit(1);
}

void printHelpFooter() {
  std::cout << std::endl;
  std::cout << "Scoring units:" << std::endl;
  std::cout << "  Scoring is parameterized to produce output in kJ/mol." << std::endl;
  std::cout << "  Output fields ending in .KCAL are recalculated to" << std::endl;
  std::cout << "  kcal/mol for ease of use and comparison of results." << std::endl;
  std::cout << "  As a reminder, the output does NOT reflect \"binding" << std::endl;
  std::cout << "  energy\", but rather docking score, and should be" << std::endl;
  std::cout << "  considered as such." << std::endl;
  std::cout << "                                Good luck! - CmDock team" << std::endl;

}

/////////////////////////////////////////////////////////////////////
// MAIN PROGRAM STARTS HERE
/////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[]) {
  // Handle premature termination
  signal (SIGINT, terminator);
  
  // Parse the command for later use
  std::string sCommand;
  for (int i = 0; i < argc; ++i) {
      sCommand = sCommand + argv[i] + " ";
  }

  // Handle obsolete arguments, if any
  int tIndex = 0;
  bool CFound = false;
  for (int i = 0; i < argc; i++) {
    std::string opt = argv[i];
    if (opt == "-ap" || opt == "-an" || opt == "-allH" || opt == "-cont") {
      std::cout
          << "Options -ap, -an, -allH, and -cont are no longer supported; use "
             "-P, -D, -H, and -C (respectively) instead."
          << std::endl;
      return 2;
    }
    if (opt == "-t") {
      tIndex = i;
    }
    if (opt == "-C") {
      CFound = true;
    }
  }
  if (tIndex > 0 && !CFound) {
    std::cout << "Option -t without -C is no longer supported; use -f instead."
              << std::endl;
    return 2;
  }

  std::cout.setf(std::ios_base::left, std::ios_base::adjustfield);

  // Strip off the path to the executable, leaving just the file name
  std::string strExeName(argv[0]);
  std::string::size_type i = strExeName.rfind("/");
  if (i != std::string::npos)
    strExeName.erase(0, i + 1);

  // Print a standard header
  Rbt::PrintStdHeader(std::cout, strExeName);
  std::cout << std::endl;

  cxxopts::Options options(strExeName, "Docking engine");

  // Command line arguments and default values
  cxxopts::OptionAdder adder = options.add_options();
  adder("i,input",
        "Input ligand SD file or CmZIP archive.", cxxopts::value<std::string>());
  adder("o,output", "Output file name(s) prefix. \
  (Output is in the form of an MDL SDF file. A .sdf file extension is recommended.)",
        cxxopts::value<std::string>());
  adder("z,zip", "Compress output file while writing."
#ifdef BUNDLE_FULL_LIBRARY
  , cxxopts::value<unsigned int>()->implicit_value("5")
#endif
  );      
  adder("c,checkpoint", "Run in checkpoint mode. \
  (In case of early termination start docking where we left off. \
  Checkpoints can be manually specified only with \
  --checkpoint=<int> )",
        cxxopts::value<std::string>()->implicit_value(""));
  adder("j,checkpoint-jump", "Specify how many records should be processed before overwriting the checkpoint file. \
  (Ex. -j 5 will write a chekcpoint for every 6th processed record, starting with 6.)", cxxopts::value<unsigned int>()->default_value("0"));
  adder("r,receptor-param", "Receptor parameter file.",
        cxxopts::value<std::string>());
  adder("p,docking-param", "Docking protocol parameter file.",
        cxxopts::value<std::string>());
  adder("n,number", "number of runs per ligand (0 = unlimited). \
  (Orientation: <10-runs quick, 20-standard, 100-exhaustive exp.)",
        cxxopts::value<unsigned int>()->default_value("0"));
  adder("b,best-poses", "Specify how many best scored poses to save. All other poses are discarded. \
  By default poses are compared via SCORE.INTER, this can be overriden \
  (syntax: -b 5,SCORE)", cxxopts::value<std::string>());
  adder("P,protonate", "Protonate all neutral amines, guanidines, imidazoles. \
  (If input ligands are not prepared; crude!)");
  adder("D,deprotonate",
        "Deprotonate all carboxylic, sulphur and phosphorous acid groups. \
  (If input ligands are not prepared; crude!)");
  adder("H,all-hydrogens",
        "Read all hydrogens present instead of only polar hydrogens. \
        (Only experimental currently!)");
  adder("t,threshold", "Score threshold.", cxxopts::value<double>());
  adder("C,continue",
        "Continue if score threshold is met instead of terminating ligand.");
  adder("f,filter", "Filter file name. \
  (Use for HTVS, perform database analysis on the docking system first!)", cxxopts::value<std::string>());
  adder("T,trace",
        "Controls output level for debugging. (0 = minimal, >0 = more verbose)",
        cxxopts::value<int>()->default_value("0"));
  adder("s,seed", "random number seed to use instead of std::random_device",
        cxxopts::value<unsigned int>());
  adder("no-log", "Disables logging to file.");
  adder("h,help", "Print help");

  try {
    auto result = options.parse(argc, argv);

    if (result.count("h")) {
      std::cout << options.help() << std::endl;
#ifdef BUNDLE_FULL_LIBRARY
      CmZ::printSupportedAlgorithms();
#endif
      printHelpFooter();
      return 0;
    }

    // Command line arguments and default values
    std::string strLigandMdlFile;
    if (result.count("i")) {
      strLigandMdlFile = result["i"].as<std::string>();
    }
    bool bOutput = result.count("o");
    std::string strRunName;
    if (bOutput) {
      strRunName = result["o"].as<std::string>();
    }
    bool bZip = result.count("z"); // Should output be compressed
    int iAlgorithm;
    if (bZip) {
#ifdef BUNDLE_FULL_LIBRARY
      iAlgorithm = CmZ::encoder(result["z"].as<unsigned int>());
#else
      iAlgorithm = CmZ::encoder(1); // Set it to LZMA20
#endif
      if (!(iAlgorithm < 24)) {
        std::cerr << "Specified compression algorithm is invalid!" << std::endl;
        return 0;
      }
    } else {
      iAlgorithm = 24;
    }
    bool bZipped = Rbt::endsWith(strLigandMdlFile, ".cmz"); // Input is compressed
    bool bCheckpoint = result.count("c");
    std::string strReceptorPrmFile;
    if (result.count("r")) {
      strReceptorPrmFile = result["r"].as<std::string>(); // Receptor param file
    }
    std::string strParamFile;
    if (result.count("p")) {
      strParamFile = result["p"].as<std::string>(); // Docking run param file
    }
    bool bFilter = result.count("f");
    std::string strFilterFile;
    if (bFilter) {
      strFilterFile = result["f"].as<std::string>(); // Filter file
    }
    bool bDockingRuns = result.count("n"); // is argument -n present?
    unsigned int nDockingRuns =
        result["n"].as<unsigned int>(); // Defaults to zero, so can detect later
                                        // whether user explictly typed -n
    bool bBestPoses = result.count("b");
    std::string strBestPoses;
    if (bBestPoses)
      strBestPoses = result["b"].as<std::string>();
    else
      strBestPoses = "0";

    bool bPosIonise = result.count("P");
    bool bNegIonise = result.count("D");
    bool bExplH = result.count("H"); // if true, read only polar hydrogens from
                                     // SD file, else read all H's present

    // Params for target score
    bool bTarget = result.count("t");
    double dTargetScore;
    if (bTarget) {
      dTargetScore = result["t"].as<double>();
    }
    bool bContinue =
        result.count("C"); // DM 25 May 2001 - if true, stop once target is met

    bool bSeed = result.count(
        "s"); // Random number seed (default = from std::random_device)
    unsigned int nSeed;
    if (bSeed) {
      nSeed = result["s"].as<unsigned int>();
    }
    bool bTrace = result.count("T");
    int iTrace = result["T"].as<int>(); // Trace level, for debugging

    // input ligand file, receptor and parameter is compulsory
    if (strLigandMdlFile.empty() || strReceptorPrmFile.empty() ||
        strParamFile.empty()) { // if any of them is missing
      std::cout << "Missing required parameter(s): -i, -r, or -p\nUse -h to open help" << std::endl;
      return 1;
    }
  
    // Open up log file
    ComposeStream log;
    std::ofstream logFile;
    if (result.count("no-log")) {
      logFile.setstate(std::ios_base::badbit);
    } else {
      logFile.open(strRunName + ".log", std::ios_base::app);
      log.linkStream(logFile);
    }
    log.linkStream(std::cout);
    logFile << "Command:" << std::endl;
    logFile << sCommand;
    logFile << std::endl;
    Rbt::PrintStdHeader(logFile, strExeName);
    logFile << std::endl;

    // print out arguments
    if (bCheckpoint) {
      log << std::endl << "Running in checkpoint mode" << std::endl;
    }
    log << std::endl << "Command line args:" << std::endl;
    log << " -i " << strLigandMdlFile << std::endl;
    log << " -r " << strReceptorPrmFile << std::endl;
    log << " -p " << strParamFile << std::endl;
    if (bZip)
      log << " -z " << iAlgorithm << " (" << CmZ::encoderName(iAlgorithm) << ")" << std::endl;
    // output is not that important but good to have
    if (!strRunName.empty()) {
      bOutput = true;
      log << " -o " << strRunName << std::endl;
    } else {
      log << "WARNING: output file name is missing." << std::endl;
    }
    // docking runs
    if (nDockingRuns >= 1) { // User typed -n explicitly
      log << " -n " << nDockingRuns << std::endl;
    } else {
      nDockingRuns = 1; // User didn't type -n explicitly, so fall back to the
                        // default of n=1
      log << " -n " << nDockingRuns << " (default) " << std::endl;
    }
    if (bBestPoses)
      log << " -b " << strBestPoses << std::endl;
    if (bSeed) // random seed (if provided)
      log << " -s " << nSeed << std::endl;
    if (bTrace) // random seed (if provided)
      log << " -T " << iTrace << std::endl;
    if (bPosIonise) // protonate
      log << " -ap " << std::endl;
    if (bNegIonise) // deprotonate
      log << " -an " << std::endl;
    if (bExplH) // all-H
      log << " -allH " << std::endl;
    if (bContinue) // stop after target
      log << " -cont " << std::endl;
    if (bTarget)
      log << " -t " << dTargetScore << std::endl;

    // BGD 26 Feb 2003 - Create filters to simulate old rbdock
    // behaviour
    std::ostringstream strFilter;
    if (!bFilter) {
      if (bTarget) // -t<TS>
      {
        if (!bDockingRuns) // -t<TS> only
        {
          strFilter << "0 1 - SCORE.INTER " << dTargetScore << std::endl;
        } else             // -t<TS> -n<N> need to check if -cont present
                           // for all other cases it doesn't matter
            if (bContinue) // -t<TS> -n<N> -cont
        {
          strFilter << "1 if - SCORE.NRUNS " << (nDockingRuns - 1)
                    << " 0.0 -1.0,\n1 - SCORE.INTER " << dTargetScore
                    << std::endl;
        } else // -t<TS> -n<N>
        {
          strFilter << "1 if - " << dTargetScore << " SCORE.INTER 0.0 "
                    << "if - SCORE.NRUNS " << (nDockingRuns - 1)
                    << " 0.0 -1.0,\n1 - SCORE.INTER " << dTargetScore
                    << std::endl;
        }
      }                      // no target score, no filter
      else if (bDockingRuns) // -n<N>
      {
        strFilter << "1 if - SCORE.NRUNS " << (nDockingRuns - 1)
                  << " 0.0 -1.0,\n0";
      } else // no -t no -n
      {
        strFilter << "0 0\n";
      }
    }

    // DM 20 Apr 1999 - set the auto-ionise flags
    if (bPosIonise)
      log
          << "Automatically protonating positive ionisable groups (amines, "
             "imidazoles, guanidines)"
          << std::endl;
    if (bNegIonise)
      log << "Automatically deprotonating negative ionisable groups "
                   "(carboxylic "
                   "acids, phosphates, sulphates, sulphonates)"
                << std::endl;
    if (!bExplH)
      log << "Reading polar hydrogens only from ligand SD file"
                << std::endl;
    else
      log << "Reading all hydrogens from ligand SD file" << std::endl;

    if (bTarget) {
      log << std::endl
                << "Lower target intermolecular score = " << dTargetScore
                << std::endl;
    }

    // Create a bimolecular workspace
    RbtBiMolWorkSpacePtr spWS(new RbtBiMolWorkSpace());
    // Set the workspace name to the root of the receptor .prm filename
    std::vector<std::string> componentList =
        Rbt::ConvertDelimitedStringToList(
          Rbt::GetRbtFileName("data/receptors", strReceptorPrmFile), "."
        );
    std::string wsName = componentList.front();
    spWS->SetName(wsName);

    // Read the docking protocol parameter file
    RbtParameterFileSourcePtr spParamSource(new RbtParameterFileSource(
        Rbt::GetRbtFileName("data/scripts", strParamFile)));
    // Read the receptor parameter file
    RbtParameterFileSourcePtr spRecepPrmSource(new RbtParameterFileSource(
        Rbt::GetRbtFileName("data/receptors", strReceptorPrmFile)));
    log << std::endl
              << "DOCKING PROTOCOL:" << std::endl
              << spParamSource->GetFileName() << std::endl
              << spParamSource->GetTitle() << std::endl;
    log << std::endl
              << "RECEPTOR:" << std::endl
              << spRecepPrmSource->GetFileName() << std::endl
              << spRecepPrmSource->GetTitle() << std::endl;

    // Create the scoring function from the SCORE section of the docking
    // protocol prm file Format is: SECTION SCORE
    //    INTER    RbtInterSF.prm
    //    INTRA RbtIntraSF.prm
    // END_SECTION
    //
    // Notes:
    // Section name must be SCORE. This is also the name of the root SF
    // aggregate An aggregate is created for each parameter in the section.
    // Parameter name becomes the name of the subaggregate (e.g. SCORE.INTER)
    // Parameter value is the file name for the subaggregate definition
    // Default directory is $CMDOCK_ROOT/data/sf
    RbtSFFactoryPtr spSFFactory(
        new RbtSFFactory()); // Factory class for scoring functions
    RbtSFAggPtr spSF(new RbtSFAgg(_ROOT_SF)); // Root SF aggregate
    spParamSource->SetSection(_ROOT_SF);
    std::vector<std::string> sfList(spParamSource->GetParameterList());
    // Loop over all parameters in the SCORE section
    for (std::vector<std::string>::const_iterator sfIter = sfList.begin();
         sfIter != sfList.end(); sfIter++) {
      // sfFile = file name for scoring function subaggregate
      std::string sfFile(Rbt::GetRbtFileName(
          "data/sf", spParamSource->GetParameterValueAsString(*sfIter)));
      RbtParameterFileSourcePtr spSFSource(new RbtParameterFileSource(sfFile));
      // Create and add the subaggregate
      spSF->Add(spSFFactory->CreateAggFromFile(spSFSource, *sfIter));
    }

    // Add the RESTRAINT subaggregate scoring function from any SF definitions
    // in the receptor prm file
    spSF->Add(spSFFactory->CreateAggFromFile(spRecepPrmSource, _RESTRAINT_SF));

    // Create the docking transform aggregate from the transform definitions in
    // the docking prm file
    RbtTransformFactoryPtr spTransformFactory(new RbtTransformFactory());
    spParamSource->SetSection();
    RbtTransformAggPtr spTransform(
        spTransformFactory->CreateAggFromFile(spParamSource, _ROOT_TRANSFORM));

    // Override the TRACE levels for the scoring function and transform
    // Dump details to std::cout
    // Register the scoring function and the transform with the workspace
    if (bTrace) {
      RbtRequestPtr spTraceReq(new RbtSFSetParamRequest("TRACE", iTrace));
      spSF->HandleRequest(spTraceReq);
      spTransform->HandleRequest(spTraceReq);
    }
    if (iTrace > 0) {
      log << std::endl
                << "SCORING FUNCTION DETAILS:" << std::endl
                << *spSF << std::endl;
      log << std::endl
                << "SEARCH DETAILS:" << std::endl
                << *spTransform << std::endl;
    }
    spWS->SetSF(spSF);
    spWS->SetTransform(spTransform);

    // DM 18 May 1999
    // Variants describing the library version, exe version, parameter file, and
    // current directory Will be stored in the ligand SD files
    RbtVariant vLib(Rbt::GetProduct() + "/" + Rbt::GetVersion());
    RbtVariant vExe(strExeName + "/" + Rbt::GetVersion());
    RbtVariant vRecep(spRecepPrmSource->GetFileName());
    RbtVariant vPrm(spParamSource->GetFileName());
    RbtVariant vDir(Rbt::GetCurrentWorkingDirectory());

    spRecepPrmSource->SetSection();
    // Read docking site from file and register with workspace
    std::string strASFile = spWS->GetName() + ".as";
    std::string strInputFile = Rbt::GetRbtFileName("data/grids", strASFile);
    // DM 26 Sep 2000 - std::ios_base::binary is invalid with IRIX CC
#if defined(__sgi) && !defined(__GNUC__)
    std::ifstream istr(strInputFile.c_str(), std::ios_base::in);
#else
    std::ifstream istr(strInputFile.c_str(),
                       std::ios_base::in | std::ios_base::binary);
#endif
    // DM 14 June 2006 - bug fix to one of the longest standing rDock issues
    //(the cryptic "Error reading from input stream" message, if cavity file was
    // missing)
    if (!istr) {
      std::string message = "Cavity file (" + strASFile +
                            ") not found in current directory or $CMDOCK_HOME";
      message += " - run cmcavity first";
      throw RbtFileReadError(_WHERE_, message);
    }
    RbtDockingSitePtr spDS(new RbtDockingSite(istr));
    istr.close();
    spWS->SetDockingSite(spDS);
    log << std::endl
              << "DOCKING SITE" << std::endl
              << (*spDS) << std::endl;

    // Prepare the SD file sink for saving the docked conformations for each
    // ligand DM 3 Dec 1999 - replaced ostrstream with RbtString in determining
    // SD file name SRC 2014 moved here this block to allow WRITE_ERROR TRUE
    std::string strOutputName;
    if (bZip) {
      strOutputName = strRunName + ".cmz";
    } else {
      strOutputName = strRunName;
    }
    if (bOutput) {
      RbtMdlFileSinkPtr spMdlFileSink(
          new RbtMdlFileSink(strOutputName, RbtModelPtr(), bCheckpoint));
      spMdlFileSink->SetZip(bZip, iAlgorithm);
      if (bBestPoses)
        spMdlFileSink->SetBestPoses(strBestPoses);
      spWS->SetSink(spMdlFileSink);
    }

    RbtPRMFactory prmFactory(spRecepPrmSource, spDS);
    prmFactory.SetTrace(iTrace);
    // Create the receptor model from the file names in the receptor parameter
    // file
    RbtModelPtr spReceptor = prmFactory.CreateReceptor();
    spWS->SetReceptor(spReceptor);

    // Register any solvent
    RbtModelList solventList = prmFactory.CreateSolvent();
    spWS->SetSolvent(solventList);
    if (spWS->hasSolvent()) {
      int nSolvent = spWS->GetSolvent().size();
      log << std::endl
                << nSolvent << " solvent molecules registered" << std::endl;
    } else {
      log << std::endl << "No solvent" << std::endl;
    }

    // SRC 2014 removed sector bOutput from here to some blocks above, for
    // WRITEERRORS TRUE

    // Seed the random number generator
    RbtRand &theRand = Rbt::GetRbtRand(); // ref to random number generator
    if (bSeed) {
      theRand.Seed(nSeed);
    }

    // Create the filter object for controlling early termination of protocol
    RbtFilterPtr spfilter;
    if (bFilter) {
      spfilter = new RbtFilter(strFilterFile);
      if (bDockingRuns) {
        spfilter->SetMaxNRuns(nDockingRuns);
      }
    } else {
      spfilter = new RbtFilter(strFilter.str(), true);
    }
    if (bTrace) {
      RbtRequestPtr spTraceReq(new RbtSFSetParamRequest("TRACE", iTrace));
      spfilter->HandleRequest(spTraceReq);
    }

    // Register the Filter with the workspace
    spWS->SetFilter(spfilter);

    // MAIN LOOP OVER LIGAND RECORDS
    // DM 20 Apr 1999 - add explicit bPosIonise and bNegIonise flags to
    // MdlFileSource constructor
    // GT 20 Apr 2021 - add checkpoints to continue where we left off
    RbtMolecularFileSourcePtr spMdlFileSource(new RbtMdlFileSource(
        strLigandMdlFile, bPosIonise, bNegIonise, !bExplH));
    spMdlFileSource->SetZipped(bZipped);
    std::chrono::duration<double> totalDuration(0.0);
    std::size_t nFailedLigands = 0;
    std::size_t nUnnamedLigands = 0;
    std::chrono::system_clock::time_point loopBegin =
        std::chrono::system_clock::now();
    std::size_t nRec;
    // Handle checkpoints at the users request
    int nRecCheckpoint;
    std::string nRecCheckpointName;
    if (bCheckpoint) {
      std::string checkpointResult;
      checkpointResult = result["c"].as<std::string>();
      if (checkpointResult.empty()) {
        if (!strRunName.empty() && Rbt::FileExists(strRunName + ".chk")) {
          std::ifstream checkpointFile(strRunName + ".chk");
          try {
            checkpointFile >> nRecCheckpoint >> nRecCheckpointName;
          } catch ( ... ) {
            log << "Checkpoint file is corrupt, ignoring." << std::endl;
            nRecCheckpoint = 1;
          }
        } else {
          nRecCheckpoint = 1;
        }
      } else {
        try {
          nRecCheckpoint = std::stoi(checkpointResult);
        } catch ( ... ) {
          log << "Checkpoint must be valid integer." << std::endl;
          return 0;
        }
      }
      // Check the integrity of the output file from the aborted run
      // This doesn't clean the file, however, it does prevent additional corrupt records
      if (!bZip) { // This is not needed if the output is zipped
        if (Rbt::FileExists(strRunName) && !(Rbt::LastLineInFile(strRunName) == "$$$$")) {
          std::fstream file;
          file.open(strRunName, std::fstream::app);
          file << std::endl << "$$$$" << std::endl;
          file.close();
        }
      }
    } else {
      nRecCheckpoint = 1;
    }
    // Main loop start
    // But first open the checkpoint file
    log << "Starting docking engine..." << std::endl;
    std::ofstream checkpointFile;
    checkpointFile.open(strRunName + ".chk");
    std::ofstream fraction_done_filename;
    fraction_done_filename.open(strRunName + ".progress");
    unsigned int jump = 0;
    unsigned int iJump = result["j"].as<unsigned int>();
    indicators::show_console_cursor(false);
#ifdef _WIN32
    indicators::ProgressBar progressBar{
#else
    indicators::BlockProgressBar progressBar{
#endif
      indicators::option::BarWidth{35},
      indicators::option::Start{"["},
      indicators::option::End{"]"},
      indicators::option::ShowRemainingTime{true}
    };
    progressBar.set_progress(0);
    for (nRec = 1; spMdlFileSource->FileStatusOK();
         spMdlFileSource->NextRecord(), nRec++) {
      if (nRec < nRecCheckpoint) {
        continue;
      }
      logFile << std::endl
                << "**************************************************"
                << std::endl
                << "RECORD #" << nRec << std::endl;
      RbtError molStatus = spMdlFileSource->Status();
      if (!molStatus.isOK()) {
        logFile << std::endl
                  << molStatus << std::endl
                  << "************************************************"
                  << std::endl;
        continue;
      }

      auto startTime = std::chrono::high_resolution_clock::now();
      bool bLigandError = false;

      // DM 26 Jul 1999 - only read the largest segment (guaranteed to be called
      // H) BGD 07 Oct 2002 - catching errors created by the ligands, so cmdock
      // continues with the next one, instead of completely stopping
      try {
        spMdlFileSource->SetSegmentFilterMap(
            Rbt::ConvertStringToSegmentMap("H"));

        if (spMdlFileSource->isDataFieldPresent("Name")) {
          RbtVariant molName = spMdlFileSource->GetDataValue("Name");
          if (molName.isEmpty()){
            nUnnamedLigands++;
          } else {
            progressBar.set_option(indicators::option::PostfixText{molName});
          }
          logFile << "NAME:   " << molName << std::endl;
        }
        if (spMdlFileSource->isDataFieldPresent("REG_Number")){
          logFile << "REG_Num:" << spMdlFileSource->GetDataValue("REG_Number")
                    << std::endl;
        }
        logFile << std::setw(30) << "RNG seed:";
        if (bSeed) {
          logFile << nSeed;
        } else {
          logFile << "std::random_device";
        }
        logFile << std::endl;

        // Create and register the ligand model
        RbtModelPtr spLigand = prmFactory.CreateLigand(spMdlFileSource);
        std::string strMolName = spLigand->GetName();
        spWS->SetLigand(spLigand);
        // Update any model coords from embedded chromosomes in the ligand file
        spWS->UpdateModelCoordsFromChromRecords(spMdlFileSource, iTrace);

        // DM 18 May 1999 - store run info in model data
        // Clear any previous Rbt.* data fields
        spLigand->ClearAllDataFields("Rbt.");
        spLigand->SetDataValue("Rbt.Library", vLib);
        spLigand->SetDataValue("Rbt.Executable", vExe);
        spLigand->SetDataValue("Rbt.Receptor", vRecep);
        spLigand->SetDataValue("Rbt.Parameter_File", vPrm);
        spLigand->SetDataValue("Rbt.Current_Directory", vDir);

        // DM 10 Dec 1999 - if in target mode, loop until target score is
        // reached
        bool bTargetMet = false;

        ////////////////////////////////////////////////////
        // MAIN LOOP OVER EACH SIMULATED ANNEALING RUN
        // Create a history file sink, just in case it's needed by any
        // of the transforms
        int iRun = 1;
        std::size_t nErrors = 0;
        // need to check this here. The termination
        // filter is only run once at least
        // one docking run has been done.
        if (nDockingRuns < 1)
          bTargetMet = true;
        while (!bTargetMet) {
          // Catching errors with this specific run
          if (nErrors > 10) {
            log << "Target not met, but giving up on ligand after "
                      << nErrors << " errors" << std::endl;
            bLigandError = true;
            break;
          }
          try {
            if (bOutput) {
              std::ostringstream histr;
              histr << strRunName << "_" << strMolName << nRec << "_his_"
                    << iRun;
              RbtMolecularFileSinkPtr spHistoryFileSink(
                  new RbtMdlFileSink(histr.str(), spLigand, bCheckpoint));
              spWS->SetHistorySink(spHistoryFileSink);
            }
            spWS->Run(); // Dock!
            bool bterm = spfilter->Terminate();
            bool bwrite = spfilter->Write();
            if (bterm) {
              bTargetMet = true;
            }
            if (bOutput && bwrite) {
              spWS->Save();
            }
            iRun++;
          } catch (RbtDockingError &e) {
            logFile << e << std::endl;
            nErrors++;
          }
        }
        if (bBestPoses)
          spWS->SaveBestPoses();

        // Update checkpointFile contents
        if (jump == iJump) {
          char checkpointContent[255];
          std::memset(checkpointContent, ' ', 255);
          std::string sCheckpointContent = std::to_string(nRec) + " " + strMolName;
          std::memmove(checkpointContent, sCheckpointContent.c_str(), std::min(sCheckpointContent.size(), (size_t)255));
          checkpointFile.seekp(0);
          checkpointFile.write(checkpointContent, 255);
          checkpointFile.flush();
        }

        // END OF MAIN LOOP OVER EACH SIMULATED ANNEALING RUN
        ////////////////////////////////////////////////////

        // here we use iRun - 1 since iRun got incremented in the last iteration
        logFile << "Numer of docking runs done:   " << iRun - 1 << " ("
                  << nErrors << " errors)" << std::endl;
      }
      // END OF TRY
      catch (RbtLigandError &e) {
        logFile << e << std::endl;
        bLigandError = true;
      }

      std::size_t estNumRecords = spMdlFileSource->GetEstimatedNumRecords();
      if (!bLigandError) {
        auto endTime = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> recordDuration = endTime - startTime;
        logFile << "Ligand docking duration:      " << recordDuration.count()
                  << " second(s)" << std::endl;
        totalDuration += recordDuration;
        // report average every 10th record starting from the 1st
        if ((nRec - nRecCheckpoint) % 10 == 1) {
          logFile << std::endl
                    << "Average duration per ligand:  "
                    << totalDuration.count() / static_cast<double>(nRec - nRecCheckpoint + 1)
                    << " second(s)" << std::endl;
          std::size_t estNumRecords = spMdlFileSource->GetEstimatedNumRecords();
          if (estNumRecords > 0) {
            std::chrono::duration<double> estimatedTimeRemaining =
                estNumRecords * (totalDuration / static_cast<double>(nRec - nRecCheckpoint + 1));
            std::chrono::system_clock::time_point loopEnd =
                loopBegin + std::chrono::duration_cast<std::chrono::seconds>(
                                estimatedTimeRemaining);
            std::time_t loopEndTime =
                std::chrono::system_clock::to_time_t(loopEnd);
            logFile << "Approximately " << estNumRecords - nRec
                      << " record(s) remaining, will finish "
                      << std::put_time(std::localtime(&loopEndTime), "%c")
                      << std::endl;
          }
        }
      } else {
        nFailedLigands++;
      }
      
      double progress = static_cast<double>(nRec)/static_cast<double>(estNumRecords);

      // pschoefer and walli, BOINC community: write fraction done to file after every record
      // for easier tracking from BOINC
      if (jump == iJump) {
        char cProgress[] = "00000000";
        std::string sProgress = std::to_string(progress);
        std::memmove(cProgress, sProgress.c_str(), std::min(sProgress.size(), (size_t)8));
        fraction_done_filename.seekp(0);
        fraction_done_filename.write(cProgress, 8);
        fraction_done_filename.flush();
        jump = 0;
      } else {
        jump++;
      }

      // Update progressBar
      progress = (static_cast<double>(nRec) - nRecCheckpoint)/(static_cast<double>(estNumRecords) - nRecCheckpoint);
      progressBar.set_progress(progress * 100);
    }
    progressBar.set_option(indicators::option::PostfixText{""});
    progressBar.set_option(indicators::option::ShowRemainingTime{false});
    progressBar.set_progress(100);
    // Remove the checkpoint file
    fraction_done_filename.close();
    std::remove((strRunName + ".progress").c_str());
    checkpointFile.close();
    std::remove((strRunName + ".chk").c_str());
    indicators::show_console_cursor(true);

    // END OF MAIN LOOP OVER LIGAND RECORDS
    ////////////////////////////////////////////////////

    log << std::endl
              << "**************************************************"
              << std::endl
              << "Total number of ligands: " << nRec - nRecCheckpoint;
    if (nFailedLigands > 0)
      log << ", of which" << nFailedLigands << " failed to dock"
                << std::endl;
    else
      log << ", all ligands docked without errors" << std::endl;

    // here we use nRec - 1 for the number of ligands since nRec got incremented
    // in the iteration in which spMdlFileSource->FileStatusOK() returned false
    // and for loop ended
    if (nRec - 1 - nFailedLigands > 0) {
      auto hTotal =
          std::chrono::duration_cast<std::chrono::hours>(totalDuration);
      totalDuration -= hTotal;
      auto mTotal =
          std::chrono::duration_cast<std::chrono::minutes>(totalDuration);
      totalDuration -= mTotal;

      log << "Docking duration for " << nRec - nRecCheckpoint - nFailedLigands
                << " ligand(s): ";

      if (hTotal.count() > 0) {
        log << hTotal.count() << " hour(s), ";
      }
      if (hTotal.count() > 0 || mTotal.count() > 0) {
        log << mTotal.count() << " minute(s), ";
      }
      log << totalDuration.count() << " second(s)" << std::endl;
    }

    if (nUnnamedLigands > 0) {
      log
          << std::endl
          << "WARNING: " << nUnnamedLigands
          << " ligand(s) are unnamed. Post-processing tools might have an "
             "issue correctly identifying different poses of the same ligand."
          << std::endl;
    }

    log << std::endl << "END OF RUN" << std::endl;
    //    if (bOutput && flexRec) {
    //      RbtMolecularFileSinkPtr spRecepSink(new
    //      RbtCrdFileSink(strRunName+".crd",spReceptor));
    //      spRecepSink->Render();
    //    }
    log << std::endl;
    log << "********************************************************" << std::endl;
    log << "Please cite the following: " << std::endl;
    Rbt::PrintBibliographyItem(log, "RiboDock2004");
    Rbt::PrintBibliographyItem(log, "rDock2014");
#if !defined(__sun) && !defined(_MSC_VER)
    Rbt::PrintBibliographyItem(log, "PCG2014");
#endif
    Rbt::PrintBibliographyItem(log, "CmDock");
    log << "********************************************************" << std::endl;
    log << std::endl;
    log << "Thank you for using " << Rbt::GetProgramName() << " "
              << Rbt::GetVersion() << "." << std::endl;
    logFile.close();
  } catch (const cxxopts::OptionException &e) {
    std::cout << "Error parsing options: " << e.what() << std::endl;
    return 1;
  } catch (RbtError &e) {
    std::cout << e << std::endl;
  } catch (const std::exception &e) {
    std::cout << "Unknown exception " << e.what() << std::endl;
  }

  _RBTOBJECTCOUNTER_DUMP_(std::cout)

  return 0;
}