#include <stdio.h>
#include <signal.h>
#include <iostream>
#include <fstream>
#include <cxxopts.hpp>
#ifdef _WIN32
#include <indicators/progress_bar.hpp>
#else
#include <indicators/block_progress_bar.hpp>
#endif
#include <indicators/cursor_control.hpp>

#include "Rbt.h"
#include "RbtCompression.hpp"

void terminator(int s) {
  std::cout << std::endl << "Caught termination signal, wrapping up..." << std::endl;
  indicators::show_console_cursor(true);
  exit(1);
}

// Decompression mode entry point
void unzip(cxxopts::ParseResult result) {

    indicators::show_console_cursor(false);
    double progress = 0.0f;
#ifdef _WIN32
    indicators::ProgressBar progressBar{
#else
    indicators::BlockProgressBar progressBar{
#endif
        indicators::option::BarWidth{35},
        indicators::option::Start{"["},
        indicators::option::End{"]"},
        indicators::option::ShowRemainingTime{true},
        indicators::option::PostfixText{"Initializing"}
    };

    // Parse input and output
    std::string strInput = result["i"].as<std::vector<std::string>>()[0];
    std::string strOutput = result["o"].as<std::string>();
    
    // Initalize archive struct and start opening files...
    CmZ::archive cmzip(DECOMPRESS);
    cmzip.inputFile.open(strInput, std::ios::binary);
    cmzip.outputFile.open(strOutput, std::ios::out | std::ios::binary);

    progressBar.set_option(indicators::option::PostfixText{"Reading index"});

    if (!cmzip.readCompressedIndex()) {
        eprintln("Error reading compressed index!");
        indicators::show_console_cursor(true);
        exit(1);
    }
    unsigned iIndexAlgorithm = CmZ::findAlgorithm(cmzip.compressedIndex);
    if (!CmZ::isAlgorithmSupported(iIndexAlgorithm)) {
        eprintln("Current build does not support " << CmZ::encoderName(iIndexAlgorithm));
        indicators::show_console_cursor(true);
        exit(1);
    }
    if (!cmzip.decompressIndex()) {
        eprintln("Error decompressing index!");
        indicators::show_console_cursor(true);
        exit(1);
    }

    // Fill vector with indices to read
    std::vector<uint64_t> tRecords;
    if (result.count("r")) {
        tRecords = result["r"].as<std::vector<uint64_t>>();
    } else {
        for (size_t i = 0; i < cmzip.index.size() - 1 ; i ++) {
            tRecords.push_back(i);
        }
    }

    progressBar.set_option(indicators::option::PostfixText{"Decompressing"});

    // Iterate over appropriate records
    uint64_t i = 0;
    uint64_t nRecLen = tRecords.size();
    uint64_t errors = 0;
    for (auto nRec : tRecords) {
        if (!cmzip.readRecordAtIndex(nRec)) {
            eprintln("Error reading record " << nRec << "!");
        }
        if (CmZ::isAlgorithmSupported(CmZ::findAlgorithm(cmzip.compressedRecord))) {
            if (!cmzip.decompress()) {
                eprintln("Error decompressing record" << nRec << "!");
            } 
            if (!cmzip.writeRecordToFile()) {
                eprintln("Error writing to file!");
            }
        } else {
            errors++;
        }
        i ++;
        progress = static_cast<double>(i) / static_cast<double>(nRecLen);
        progressBar.set_progress(progress * 100);
    }
    
    progressBar.set_option(indicators::option::PostfixText{"Done!"});
    progressBar.set_progress(100);
    indicators::show_console_cursor(true);

    if (errors > 0) {
        std::cout << errors << " could not be decompressed!" << std::endl;
        std::cout << "Their algorithms are not supported in this build." << std::endl;
    }

}

// Compression mode entry mode
void zip(cxxopts::ParseResult result) {
    
    indicators::show_console_cursor(false);
    double progress = 0;
#ifdef _WIN32
    indicators::ProgressBar progressBar{
#else
    indicators::BlockProgressBar progressBar{
#endif
        indicators::option::BarWidth{35},
        indicators::option::Start{"["},
        indicators::option::End{"]"},
        indicators::option::ShowRemainingTime{true},
        indicators::option::PostfixText{"Initializing"}
    };

    // Setup variables from command line input
    std::string strInput = result["i"].as<std::vector<std::string>>()[0];
    std::string strOutput = result["o"].as<std::string>();
    unsigned algorithm = CmZ::encoder(result["a"].as<unsigned>());
    if (!(algorithm < 24)) {
        eprintln("Specifed algorithm is invalid!");
        indicators::show_console_cursor(true);
        exit(1);
    }
    // Always end archive file names with .cmz
    if (!Rbt::endsWith(strOutput, ".cmz")) {
        strOutput = strOutput + ".cmz";
    }

    // Initialize input and output file streams
    CmZ::archive cmzip(algorithm);
    cmzip.inputFile.open(strInput);
    cmzip.outputFile.open(strOutput, std::ios::out | std::ios::binary);

    progressBar.set_option(indicators::option::PostfixText{"Compressing"});
    cmzip.inputFile.seekg(0, std::ios_base::end);
    uint64_t fileSizeProgress = cmzip.inputFile.tellg();
    cmzip.inputFile.seekg(0);

    // Create vectors ued in compression sequence
    std::string line;
    std::string delimiter = "$$$$";
    while (std::getline(cmzip.inputFile, line)) { // Iterate over lines in file
        std::string cLine = line + "\n"; 
        if (cLine.rfind(delimiter, 0) != 0) {
            cmzip.decompressedRecord.insert(std::end(cmzip.decompressedRecord), cLine.c_str(), cLine.c_str() + strlen(cLine.c_str()));
            continue; // Only finish loop when $$$$ is reached, this way we compress each record by itself
        }
        cmzip.decompressedRecord.insert(std::end(cmzip.decompressedRecord), cLine.c_str(), cLine.c_str() + strlen(cLine.c_str()));
        
        // Compress record with specified compression level
        if (!cmzip.compress()) {
            eprintln("Error compressing record!");
            indicators::show_console_cursor(true);
            exit(1);
        };
        cmzip.decompressedRecord.clear();

        // Add compressed record to index
        if (!cmzip.addCurrentToIndex()) {
            eprintln("Error adding to index!");
            indicators::show_console_cursor(true);
            exit(1);
        };

        // Write record to file
        if (!cmzip.writeRecordToFile()) {
            eprintln("Error writing to file!");
            indicators::show_console_cursor(true);
            exit(1);
        };
        
        uint64_t currentBit = cmzip.inputFile.tellg();
        progress = static_cast<double>(currentBit) / static_cast<double>(fileSizeProgress);
        progressBar.set_progress(progress * 100);

    }
    
    progressBar.set_option(indicators::option::PostfixText{"Done!"});
    progressBar.set_progress(100);
    indicators::show_console_cursor(true);
    
}

void join(cxxopts::ParseResult result){

    indicators::show_console_cursor(false);
#ifdef _WIN32
    indicators::ProgressBar progressBar{
#else
    indicators::BlockProgressBar progressBar{
#endif
        indicators::option::BarWidth{35},
        indicators::option::Start{"["},
        indicators::option::End{"]"},
        indicators::option::ShowRemainingTime{true},
        indicators::option::PostfixText{"Initializing"}
    };

    // Setup variables from command line input
    std::vector<std::string> vArchives = result["i"].as<std::vector<std::string>>();
    std::string strInput = vArchives[0];
    std::string strOutput;
    bool bOutput = result.count("o");

    // Use input as output if no output specified
    if (bOutput) {
        strOutput = result["o"].as<std::string>();
    } else {
        strOutput = strInput;
    }
    
    // Always end archive file names with .cmz
    if (!Rbt::endsWith(strOutput, ".cmz")) {
        strOutput = strOutput + ".cmz";
    }

    // Open strInput with DECOMPRESS just to find algorithm
    CmZ::archive aDestination(DECOMPRESS);
    aDestination.inputFile.open(strInput);
    if (!aDestination.readCompressedIndex()) {
        eprintln("Error reading compressed index!");
        indicators::show_console_cursor(true);
        exit(1);
    }
    int algorithm = CmZ::findAlgorithm(aDestination.compressedIndex);
    if (CmZ::isAlgorithmSupported(algorithm)) {
        eprintln("Current build does not support " << CmZ::encoderName(algorithm));
        indicators::show_console_cursor(true);
        exit(1);
    }
    aDestination.mode = algorithm;
    
    float nFiles = vArchives.size();

    if (bOutput) { // If output is specified we need to copy the input file
        aDestination.index = {0};
        aDestination.outputFile.open(strOutput, std::ios::out | std::ios::binary);
    } else {
        vArchives.erase(vArchives.begin());
        if (!aDestination.decompressIndex()) {
            eprintln("Error decompressing index!");
            indicators::show_console_cursor(true);
            exit(1);
        }
        aDestination.inputFile.close();
        aDestination.outputFile.open(strOutput, std::ios::in | std::ios::out | std::ios::binary);
        aDestination.outputFile.seekp(aDestination.calculateOffset(UINT64_MAX));
    }

    progressBar.set_option(indicators::option::PostfixText{"Concatenating"});
    float i = 0.0;
    for (auto strArchive : vArchives) {
        CmZ::archive aSource(DECOMPRESS);
        aSource.inputFile.open(strArchive);
        
        if (!aSource.readCompressedIndex()) {
            eprintln("Error reading index!");
            indicators::show_console_cursor(true);
            exit(1);
        }
        unsigned iIndexAlgorithm = CmZ::findAlgorithm(aSource.compressedIndex);
        if (!CmZ::isAlgorithmSupported(iIndexAlgorithm)) {
            eprintln("Current build does not support " << CmZ::encoderName(iIndexAlgorithm));
            indicators::show_console_cursor(true);
            exit(1);
        }
        if (!aSource.decompressIndex()) {
            eprintln("Error decompressing index!");
            indicators::show_console_cursor(true);
            exit(1);
        }

        for (uint64_t j = 0; j < (aSource.index.size() - 1); j ++) {
            if (!aSource.readRecordAtIndex(j)) {
                eprintln("Error reading record " << j << "in file" << i << "!");
                indicators::show_console_cursor(true);
                exit(1);
            }
            aDestination.index.push_back(aSource.compressedRecord.size());
            aDestination.outputFile.write((char*) aSource.compressedRecord.data(), aSource.compressedRecord.size());
        }

        i ++;
        progressBar.set_progress((i / nFiles) * 100);
    }

    progressBar.set_option(indicators::option::PostfixText{"Done!"});
    progressBar.set_progress(100);
    indicators::show_console_cursor(true);

}

void split(cxxopts::ParseResult result) {
    indicators::show_console_cursor(false);
#ifdef _WIN32
    indicators::ProgressBar progressBar{
#else
    indicators::BlockProgressBar progressBar{
#endif
        indicators::option::BarWidth{35},
        indicators::option::Start{"["},
        indicators::option::End{"]"},
        indicators::option::ShowRemainingTime{true},
        indicators::option::PostfixText{"Initializing"}
    };
    // Setup variables from command line input
    std::string strInput = result["i"].as<std::vector<std::string>>()[0];
    std::string strOutput = result["o"].as<std::string>();

    CmZ::archive aInput;
    aInput.inputFile.open(strInput);
    aInput.readCompressedIndex();
    unsigned iIndexAlgorithm = CmZ::findAlgorithm(aInput.compressedIndex);
    if (!CmZ::isAlgorithmSupported(iIndexAlgorithm)) {
        eprintln("Current build does not support " << CmZ::encoderName(iIndexAlgorithm));
        indicators::show_console_cursor(true);
        exit(1);
    }
    aInput.decompressIndex();
    int iAlgorithm = CmZ::findAlgorithm(aInput.compressedIndex);

    // Get number of records in file
    uint64_t iRecs = aInput.index.size() - 1;

    progressBar.set_option(indicators::option::PostfixText{"Splitting"});
    if (result.count("f")) {  // Split by number of files
        // Get number of files
        uint64_t iFiles = result["f"].as<uint64_t>();

        // Caluclate number of CmZ Archives per file
        uint64_t iDiv = iRecs / iFiles;
        uint64_t iRem = iRecs % iFiles;

        // Stoe number of CmZ Archives in Vector<uint64_t>
        std::vector<uint64_t> vSizes = {};
        for (uint64_t i = 0; i < iFiles; i++) {
            if (i < iRem) {
                vSizes.push_back(iDiv + 1);
            } else {
                vSizes.push_back(iDiv);
            }
        }

        // Iterate over vector
        uint64_t iCurrent = 1;
        for (uint64_t iSize : vSizes) {
            progressBar.set_option(indicators::option::PostfixText{"File #: " + std::to_string(iCurrent)});
            CmZ::archive aOutput(iAlgorithm);
            std::string filename = strOutput + "_" + std::to_string(iCurrent) + ".cmz";
            aOutput.outputFile.open(filename, std::ios_base::out | std::ios_base::binary);
            for (uint64_t i = 0; i < iSize; i++) {
                aInput.readNextRecord();
                uint64_t iRecSize = aInput.compressedRecord.size();
                aOutput.compressedRecord.resize(iRecSize);
                std::memmove(aOutput.compressedRecord.data(), aInput.compressedRecord.data(), iRecSize);
                aOutput.addCurrentToIndex();
                aOutput.writeRecordToFile();
                progressBar.set_progress(((double)aInput.nextRecord / (double)iRecs) * 100);
            }
            iCurrent++;
        }

    } else if (result.count("s")) { // Split by number of records
        uint64_t iSize = result["s"].as<uint64_t>();
        uint64_t iCurrent = 1;
        while (aInput.nextRecord < aInput.index.size() - 1) {
            progressBar.set_option(indicators::option::PostfixText{"File #: " + std::to_string(iCurrent)});
            CmZ::archive aOutput(iAlgorithm);
            std::string filename = strOutput + "_" + std::to_string(iCurrent) + ".cmz";
            aOutput.outputFile.open(filename, std::ios_base::out | std::ios_base::binary);
            for (uint64_t i = 0; i < iSize; i++) {
                aInput.readNextRecord();
                uint64_t iRecSize = aInput.compressedRecord.size();
                aOutput.compressedRecord.resize(iRecSize);
                std::memmove(aOutput.compressedRecord.data(), aInput.compressedRecord.data(), iRecSize);
                aOutput.addCurrentToIndex();
                aOutput.writeRecordToFile();
                progressBar.set_progress(((double)aInput.nextRecord / (double)iRecs) * 100);
                if (aInput.nextRecord == aInput.index.size() - 1)
                    break;
            }
            iCurrent++;
        }
    }
    progressBar.set_option(indicators::option::PostfixText{"Done!"});
    progressBar.set_progress(100);
    indicators::show_console_cursor(true);
}

void rescue(cxxopts::ParseResult result){
    indicators::show_console_cursor(false);
#ifdef _WIN32
    indicators::ProgressBar progressBar{
#else
    indicators::BlockProgressBar progressBar{
#endif
        indicators::option::BarWidth{35},
        indicators::option::Start{"["},
        indicators::option::End{"]"},
        indicators::option::ShowRemainingTime{true},
        indicators::option::PostfixText{"Initializing"}
    };
    // Setup variables from command line input
    std::string strInput = result["i"].as<std::vector<std::string>>()[0];
    bool bOutput = result.count("o");
    std::string strOutput;
    // Use input as output if no output specified
    if (bOutput) {
        strOutput = result["o"].as<std::string>();
    } else {
        strOutput = strInput;
    }
    
    // Always end archive file names with .cmz
    if (!Rbt::endsWith(strOutput, ".cmz")) {
        strOutput = strOutput + ".cmz";
    }

    // If output is specified, copy the file contents
    progressBar.set_option(indicators::option::PostfixText{"Copying file"});
    if (bOutput) {
        std::ifstream source(strInput, std::ios_base::binary);
        std::ofstream destination(strOutput, std::ios_base::binary);
        destination << source.rdbuf();
        source.close();
        destination.close();
    }

    CmZ::archive cmzip;

    progressBar.set_option(indicators::option::PostfixText{"Searching..."});
    cmzip.index = CmZ::rebuildIndex(strInput, progressBar);
    progressBar.set_option(indicators::option::PostfixText{"Analyzing"});
    
    // Check if any records were found
    if (cmzip.index.size() < 2) {
        progressBar.set_option(indicators::option::PostfixText{"Done"});
        progressBar.set_progress(100);
        indicators::show_console_cursor(true);
        eprintln("No valid compressed record found!");
        exit(0);
    }

    // Check if any records are valid
    cmzip.inputFile.open(strInput, std::ios_base::binary);
    if (!(cmzip.readRecordAtIndex(0) && 
        cmzip.decompress())) {
        progressBar.set_option(indicators::option::PostfixText{"Done"});
        progressBar.set_progress(100);
        indicators::show_console_cursor(true);
        eprintln("No valid compressed record found!");
        exit(0);
    }

    // Check if last index entry points to a record or index
    if (!(cmzip.readRecordAtIndex(UINT64_MAX) &&
          cmzip.decompress())) {
        progressBar.set_option(indicators::option::PostfixText{"Done"});
        progressBar.set_progress(100);
        indicators::show_console_cursor(true);
        eprintln("Error reading last record!");
        exit(0);
    }
    bool bIsValidMdlHeader = CmZ::isValidMdlHeader(cmzip.decompressedRecord);
    cmzip.index.pop_back();

    cmzip.inputFile.close();
    
    int iAlgorithm = CmZ::findAlgorithm(cmzip.compressedRecord);
    switch (iAlgorithm) {
        case INT_MAX:
            cmzip.mode = 5;
            break;
        default:
            cmzip.mode = iAlgorithm;
    }
    cmzip.compressIndex();

    cmzip.outputFile.open(strOutput, std::ios_base::in | std::ios_base::out | std::ios_base::binary);
    if (!bIsValidMdlHeader) {
        cmzip.outputFile.seekp(cmzip.calculateOffset(UINT64_MAX), std::ios_base::beg);
    } else {
        cmzip.outputFile.seekp(0, std::ios_base::end);
    }
    progressBar.set_option(indicators::option::PostfixText{"Done"});
    progressBar.set_progress(100);
    indicators::show_console_cursor(true);
    std::cout << "Found " << cmzip.index.size() - 1 << " records!" << std::endl;
}

// ENTRY POINT
int main(int argc, char *argv[]) {
  // Handle premature termination
  signal (SIGINT, terminator);

  std::cout.setf(std::ios_base::left, std::ios_base::adjustfield);

  // Print a standard header
  Rbt::PrintStdHeader(std::cout, "CmZIP");
  std::cout << std::endl;

  cxxopts::Options options("cmzip", "CmDock archive utility.\n  MDL SD file records are encoded individually and concatenated\n  into a file. CmZ archives also contain a file footer which\n  allows for individual decompression and easier processing.\n");
  options.custom_help("-Z/U/C/R [-a/r <INT>] -i <FILE> -o <FILE>");
  cxxopts::OptionAdder adderZ = options.add_options("Compression mode");
  adderZ("Z,zip", "Runs tool in compression mode");
  adderZ("a,algorithm", "Sets the compression algorithm", cxxopts::value<unsigned>()->default_value("5"));

  cxxopts::OptionAdder adderC = options.add_options("Concatenation mode");
  adderC("C,concatenate", "Concatenates specified archives in order of input. If output isn't specified the first file is overwritten.");

  cxxopts::OptionAdder adderS = options.add_options("Split mode");
  adderS("S,split", "Splits file into multiple subfiles by size or number of files");
  adderS("f,number-of-files", "Specifies number of subfiles to create", cxxopts::value<uint64_t>());
  adderS("s,size-of-files", "Specifies number of records to save in each file", cxxopts::value<uint64_t>());

  cxxopts::OptionAdder adderU = options.add_options("Decompression mode");
  adderU("U,unzip", "Runs tool in decompression mode");
  adderU("r,records", "Comma separated records to extract starting at 0", cxxopts::value<std::vector<uint64_t>>());

  cxxopts::OptionAdder adderR = options.add_options("Rescue mode");
  adderR("R,rescue", "Runs tool in rescue mode. \
Use this if CmDock terminates prematurely. CmZIP will search for records in the file and will attempt to rebuild it's index. The resulting archive will be written to output and can then be decompressed with CmZIP.");

  cxxopts::OptionAdder adder = options.add_options();
  adder("i,input", "Specifies input file", cxxopts::value<std::vector<std::string>>());
  adder("o,output", "Specifies output file", cxxopts::value<std::string>());
  adder("h,help", "Print help");

  try {
    auto result = options.parse(argc, argv);

    // Print help
    if (result.count("h")) {
      std::cout << options.help() << std::endl;
#ifdef BUNDLE_FULL_LIBRARY
      CmZ::printSupportedAlgorithms();
#endif
      return 0;
    }

    // Parse results
    bool bInput = result.count("i");
    bool bOutput = result.count("o"); 
    if (!bInput && !bOutput) {
        std::cout << "ERR: I/O files not specified." << std::endl;
        return 0;
    }

    // Run program in appropriate mode
    if (result.count("Z") + result.count("U") + result.count("C") + result.count("R") + result.count("S") != 1) {
      eprintln("Please specify exacly one operating mode!");
      return 0;
    } else if (result.count("Z")) {
      zip(result);
    } else if (result.count("U")) {
      unzip(result);
    } else if (result.count("C")) {
      join(result);
    } else if (result.count("R")) {
      rescue(result);
    } else if (result.count("S")) {
      split(result);
    } else {
      eprintln("Operating mode unspecified, terminating.");
      return 0;
    }

  } catch (const cxxopts::OptionException &e) {
    std::cout << "Error parsing options: " << e.what() << std::endl;
    return 1;
  } catch (const std::exception &exc) {
    eprintln(exc.what());
  }
  /*} catch (...) {
      std::cout << "Unknown exception" << std::endl;
  }*/
}